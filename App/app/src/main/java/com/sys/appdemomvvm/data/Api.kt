package com.sys.appdemomvvm.data

import com.sys.appdemomvvm.model.GenderDto
import com.sys.appdemomvvm.util.Constants
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

//Patron Singleton
object Api {

    //URL BASE = https://marketapp2021.herokuapp.com/
    //METODO = api/usuarios/obtener-generos
    //URL BASE + METODO = URL COMPLETA
    //https://marketapp2021.herokuapp.com/api/usuarios/obtener-generos

    private val builder : Retrofit.Builder = Retrofit.Builder().
            baseUrl(Constants.URL_BASE).
            addConverterFactory(GsonConverterFactory.create())

    interface ApiInterface{

        @GET("api/usuarios/obtener-generos")
        suspend fun getGenders() : Response<GenderDto>
    }

    fun build() : ApiInterface{
        return builder.build().create(ApiInterface::class.java)
    }

}