package com.sys.appdemomvvm.model

import com.google.gson.annotations.SerializedName

data class GenderDto(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("message")
    val message:String,
    @SerializedName("data")
    val data:List<Gender>
)