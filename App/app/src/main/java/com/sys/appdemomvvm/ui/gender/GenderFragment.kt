package com.sys.appdemomvvm.ui.gender

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.sys.appdemomvvm.R
import com.sys.appdemomvvm.data.Api
import com.sys.appdemomvvm.databinding.FragmentGenderBinding
import com.sys.appdemomvvm.model.GenderDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenderFragment : Fragment() {

    private lateinit var binding : FragmentGenderBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gender, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentGenderBinding.bind(view)

        loadData()
    }

    private fun loadData() {

        //COROUTINES + RETROFIT
        /*GlobalScope.launch(Dispatchers.Main) {

            binding.progressBar.visibility = View.VISIBLE

            try{
                val response = withContext(Dispatchers.IO){
                    Api.build().getGenders()
                }
                if(response.isSuccessful){
                    val dto = response.body()

                    dto?.data?.forEach {
                        println(it.descripcion)
                    }
                }else{
                    Toast.makeText(requireContext(),response.message(),Toast.LENGTH_SHORT).show()
                }
            }catch (ex:Exception){
                Toast.makeText(requireContext(),ex.message,Toast.LENGTH_SHORT).show()
            }finally {
                binding.progressBar.visibility = View.GONE
            }

        }*/

        /*RETROFIT
        val response = Api.build().getGenders()
        response.enqueue(object : Callback<GenderDto> {

            override fun onResponse(call: Call<GenderDto>, response: Response<GenderDto>) {

                if(response.isSuccessful){ //200

                    val dto = response.body()

                    dto?.data?.forEach {
                        println(it.descripcion)
                    }

                }else{
                    Toast.makeText(requireContext(),response.message(),Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<GenderDto>, t: Throwable) {
                Toast.makeText(requireContext(),t.message,Toast.LENGTH_SHORT).show()
            }


        })*/
    }
}